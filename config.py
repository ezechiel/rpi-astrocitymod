#DIR application
DIR_ROM = '/home/pi/astroMOD/rom/'
DIR_VIDEO = '/home/pi/astroMOD/video/'

#Time during press key action
TIME_FOR_LONG_PRESS =  0.6
TIME_VERY_LONG_QUIT =  3

#GPIO setup
BUTTON_GPIO = 12

#APPLICATION
APP_LOADED = 0 #value 0 play video, 1 start engine emulator
AUTOLOAD = 1 #value 0 no, 1 yes => load automatically program
SHUTDOWN = 'sudo shutdown -h now'