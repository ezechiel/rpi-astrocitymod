###
# Script by Ezechiel
# Date : 2019/03/07
# Description :  Play video or launch emulator witch control button
###

import os, time, psutil
import RPi.GPIO as GPIO
import subprocess, signal
from config import *

GPIO.setmode(GPIO.BCM)
GPIO.setup(BUTTON_GPIO, GPIO.IN, pull_up_down=GPIO.PUD_UP)

#object 
class PropertyAction():
	data = {}
	name_game = []
	process_id = None
	path_all_video = []
	path_all_game = []
	app_id = APP_LOADED
	first_time = 0
	last_time = 0
	
	def __init__(self, game, video):
		self.data = {'cpt_games' : 0, 'max_games' : len(game), 'cpt_video' : 0, 'max_video' : len(video)}
		
		#script Video
		for name in video:
			self.path_all_video.append('omxplayer -o local -b --loop --aspect-mode stretch ' + DIR_VIDEO + name)
			
		#script Game
		for name in game:
			self.name_game.append(name[:-4])

	#Test exist pid by name
	def check_pid(self, processName):
		#Iterate over the all the running process
		for proc in psutil.process_iter():
			try:
				if processName.lower() in proc.name().lower():
					return True
			except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
				pass				
		return False;

	#Kill unix pid by pid
	@property
	def kill_id(self):
		try:
			if self.check_pid('mame') == True:
				subprocess.call('pgrep mame | xargs kill', shell=True)
			
			if self.process_id is not None:
				os.killpg(self.process_id.pid, signal.SIGTERM)
				self.process_id = None
								
			subprocess.call('sudo clear', shell=True)
			time.sleep(0.1)
		except:
			pass
					
	#Incremente cpt for next video/roms		
	def incremente_value(self, cpt_value, max_value):
		self.data[cpt_value] += 1
		
		if self.data[cpt_value] >= self.data[max_value]:
			self.data[cpt_value] = 0
			
	#Change rom emulator
	@property
	def change_rom(self):
		self.kill_id
		
		if self.process_id is None:			
			os.popen('advmame ' + self.name_game[self.data['cpt_games']])	
			#print(self.name_game[self.data['cpt_games']])
			self.incremente_value('cpt_games', 'max_games')	
		
	#Launch play video player and change video for next video	
	@property
	def read_video(self):
		self.kill_id
		
		if self.process_id is None:
			self.process_id = subprocess.Popen(self.path_all_video[self.data['cpt_video']], shell=True, preexec_fn=os.setsid)
			#print(self.path_all_video[self.data['cpt_video']])
			self.incremente_value('cpt_video', 'max_video')		
			
	#Action GPIO_Event	
	def action_button(self, channel):
		#Detect during press key
		if GPIO.input(channel) == False:
			self.first_time = time.time()
		else:
			self.second_time = time.time()
		
			#very long press key => quit soft
			if self.second_time - self.first_time >= TIME_VERY_LONG_QUIT:
				subprocess.run(SHUTDOWN, shell=True)
			else:
				#long press key => change application
				if self.second_time - self.first_time >= TIME_FOR_LONG_PRESS:
					if self.app_id == 0:
						self.app_id = 1
						action.change_rom
					else:
						self.app_id = 0
						action.read_video
				else:
					#Short press Key
					if self.app_id == 0:
						action.read_video
					else:
						action.change_rom
	
	#Autorun program
	@property					
	def autostart(self):
		if AUTOLOAD == 1:
			if self.app_id == 0:
				action.read_video
			else:
				action.change_rom	
			
#START
try:
	action = PropertyAction(os.listdir(DIR_ROM), os.listdir(DIR_VIDEO))
	GPIO.add_event_detect(BUTTON_GPIO, GPIO.BOTH, callback=action.action_button, bouncetime=20)
	action.autostart
		
	while True:				
		time.sleep(300) #keep alive daemon in second
finally:
	GPIO.cleanup()